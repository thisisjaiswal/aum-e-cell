import React, { Component } from 'react';
import { View, Text } from 'react-native';

import CodePush from 'react-native-code-push';
import Navigator from './e_cell/Navigator';

class CodePushApp_ extends Component {
  constructor() {
    super();
    this.state = { uptoDate: false };
  }

  codePushStatusDidChange(syncStatus) {
    switch (syncStatus) {
      case CodePush.SyncStatus.UP_TO_DATE:
      case CodePush.SyncStatus.UPDATE_IGNORED:
      case CodePush.SyncStatus.UPDATE_INSTALLED:
      case CodePush.SyncStatus.UNKNOWN_ERROR:
        this.setState({ progress: false, uptoDate: true });
        break;
    }
  }

  codePushDownloadDidProgress(progress) {
    this.setState({ progress });
  }

  componentWillMount() {
    CodePush.sync(
      { installMode: CodePush.InstallMode.IMMEDIATE, updateDialog: false },
      this.codePushStatusDidChange.bind(this),
      this.codePushDownloadDidProgress.bind(this)
    );
  }

  render() {
    //return this.state.uptoDate ? <App /> : null;
    return <Navigator />;
  }
}
export default CodePush({ checkFrequency: CodePush.CheckFrequency.MANUAL })(CodePushApp_);
