import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Modal, Image, TextInput, ActivityIndicator } from 'react-native';
import GallerySwiper from 'react-native-gallery-swiper';
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UUID: '1',

      id: '',
      imageuri: '',
      ModalVisibleStatus: false,
      dataSource: [],
      dataSource2: [],
      loading: true
    };
  }

  ShowModalFunction(visible, imageURL, id) {
    //handler to handle the click on image of Grid
    //and close button on modal
    this.setState({
      ModalVisibleStatus: visible,
      imageuri: imageURL,
      id: id
    });
  }

  componentDidMount() {
    // alert(this.state.stall.UUID)
    this.fetchStallDiscount();
  }

  fetchStallDiscount() {
    fetch('https://5D2DN6ICU4-dsn.algolia.net/1/indexes/Gallery/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-Algolia-Application-Id': '5D2DN6ICU4',
        'X-Algolia-API-Key': '439030e02a665fff425bad82365ec189'
      }
    })
      .then(response => response.json())
      .then(json => {
        var item = json;
        this.setState({ dataSource: item.hits[0].dataSource });
        this.setState({ dataSource2: item.hits[0].dataSource2 });
        //alert(JSON.stringify(item.hits[0].dataSource2));
        this.setState({ loading: false });
      })
      .catch(error => {
        toastobject(error);
        this.setState({ loading: false });
      });
  }
  render() {
    if (this.state.loading === true) {
      return (
        <View
          style={{
            height: 400,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
          <ActivityIndicator size="large" color="#000000" />
        </View>
      );
    } else {
      if (this.state.ModalVisibleStatus) {
        return (
          <Modal
            transparent={false}
            animationType={'fade'}
            visible={this.state.ModalVisibleStatus}
            onRequestClose={() => {
              this.ShowModalFunction(!this.state.ModalVisibleStatus, '', '');
            }}>
            <GallerySwiper
              initialPage={100}
              images={this.state.dataSource2}
              initialNumToRender={this.state.id}
              // Turning this off will make it feel faster
              // and prevent the scroller to slow down
              // on fast swipes.
              sensitiveScroll={false}
            />
          </Modal>
        );
      } else {
        return (
          <View style={styles.container}>
            <View style={{ height: 35, backgroundColor: 'black' }}></View>
            <FlatList
              data={this.state.dataSource}
              renderItem={({ item }) => (
                <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
                  <TouchableOpacity
                    key={item.id}
                    style={{ flex: 1 }}
                    onPress={() => {
                      this.ShowModalFunction(true, item.src, item.id);
                    }}>
                    <Image
                      style={styles.image}
                      source={{
                        uri: item.src
                      }}
                    />
                  </TouchableOpacity>
                </View>
              )}
              //Setting the number of column
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        );
      }
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0
  },
  image: {
    height: 165,
    width: '100%'
  },
  fullImageStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '98%',
    resizeMode: 'contain'
  },
  modelStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)'
  },
  closeButtonStyle: {
    width: 25,
    height: 25,
    top: 9,
    right: 9,
    position: 'absolute'
  }
});
