import * as React from 'react';
import { Text, View, StyleSheet, FlatList, WebView, ListItem, List, CheckBox, Image, Button, ScrollView } from 'react-native';

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      table: [
        {
          subject: 'DS/ECE/DCCN LAB',
          timing: '9:10 to 10:010',
          facuilty: 'APYS',
          facuiltycode: '',
          rommword: 'Room :- ',
          room: 'LAB-3/2/1',
          linecirclecolor: '#23B1A5'
          //icon: require("./assets/noimage.png")
        },
        {
          subject: 'DS/ECE/DCCN LAB',
          timing: '10:10 to 11:10',
          facuilty: 'APYS',
          facuiltycode: '',
          rommword: 'Room :- ',
          room: 'LAB-3/2/1',
          linecirclecolor: '#23B1A5'
          // icon: require("./assets/noimage.png")
        },
        {
          subject: 'MATH [AM2301]',
          timing: '11:10 to 12:00',
          facuilty: 'Dr Ajit Kumar Singh',
          facuiltycode: '[26886]',
          rommword: 'Room :- ',
          room: '217',
          linecirclecolor: 'rgb(45,156,219)'
          //icon: require("./assets/maths.png")
        },

        {
          subject: 'FL',
          timing: '4:00 to 4:50',
          facuilty: 'APYS',
          facuiltycode: '[123456]',
          rommword: 'Room :- ',
          room: '217',
          linecirclecolor: 'rgb(45,156,219)'
          //icon: require("./assets/noimage.png")
        }
      ]
    };
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ backgroundColor: 'black', height: 29 }}></View>

        <WebView
          source={{ uri: 'https://ecellaum.in/articles/' }}
          style={{ marginTop: 0 }}
          javaScriptEnabled={true}
          //For the Cache
          domStorageEnabled={true}
          //View to show while loading the webpage
          renderLoading={this.ActivityIndicatorLoadingView}
          //Want to show the view or not
          startInLoadingState={true}
        />
      </View>
    );
  }
}
export default Home;
