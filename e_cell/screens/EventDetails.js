import * as React from 'react';
import { Text, View, StyleSheet, FlatList, ListItem, List, CheckBox, Image, Button, ScrollView } from 'react-native';

import { WebView } from 'react-native-webview';
import { Thumbnail, Header } from 'native-base';

export default class EventDetails extends React.Component {
  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    //alert(JSON.stringify(item.SpeakerImage));
    return (
      <View
        style={{
          flex: 1,
          paddingTop: 20,
          backgroundColor: 'white'
        }}>
        <View style={{ overflow: 'hidden', paddingBottom: 5 }}>
          <View
            style={{
              backgroundColor: '#fff',
              width: '100%',
              height: 50,
              shadowColor: '#000',
              shadowOffset: { width: 1, height: 1 },
              shadowOpacity: 0.4,
              shadowRadius: 3,
              elevation: 5,
              flexDirection: 'row'
            }}>
            <View
              style={{
                flex: 0.5,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white'
              }}></View>

            <View
              style={{
                flex: 2,
                justifyContent: 'center',
                alignItems: 'flex-start'
              }}>
              <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.Name}</Text>
            </View>
          </View>
        </View>
        <ScrollView>
          {item.SpeakerImage ? (
            <View
              style={{
                backgroundColor: '',

                alignItems: 'center'
              }}>
              <Thumbnail
                style={{
                  height: 120,
                  width: 120,
                  borderRadius: 60,
                  borderWidth: 1,
                  marginTop: 5,
                  borderColor: 'orange'
                }}
                source={{ uri: item.SpeakerImage }}
              />
            </View>
          ) : null}
          {item.SpeakerName ? (
            <View
              style={{
                backgroundColor: '',
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: ''
              }}>
              <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{item.SpeakerName}</Text>
            </View>
          ) : null}

          {item.SpeakerDesignation ? (
            <View
              style={{
                backgroundColor: '',
                height: 12,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: ''
              }}>
              <Text style={{ fontSize: 15 }}>{item.SpeakerDesignation}</Text>
            </View>
          ) : null}

          {item.SpeakerDescription ? (
            <View
              style={{
                backgroundColor: '',

                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '',
                padding: 10
              }}>
              <Text style={{ fontSize: 15, color: 'grey' }}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is
              </Text>
            </View>
          ) : null}

          <View style={{ padding: 10 }}>
            {item.Department ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,

                  backgroundColor: ''
                }}>
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                  Department:-
                  <Text style={{ fontWeight: '' }}>{'  ' + item.Department}</Text>
                </Text>
              </View>
            ) : null}
            {item.FullTiming ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,
                  marginTop: 15,
                  backgroundColor: ''
                }}>
                <Text
                  style={{
                    fontSize: 16,

                    color: 'black',
                    fontWeight: 'bold'
                  }}>
                  {'Timing:- '}
                  <Text style={{ fontWeight: '' }}>{item.FullTiming}</Text>
                </Text>
              </View>
            ) : null}

            {item.Date ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,
                  marginTop: 15,
                  backgroundColor: ''
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: 'black'
                  }}>
                  {'Date:- '}
                  <Text style={{ fontWeight: '' }}>
                    {item.Day + ', '}
                    {item.Date}
                  </Text>
                </Text>
              </View>
            ) : null}

            {item.Location ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,
                  marginTop: 15,
                  backgroundColor: ''
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: 'black'
                  }}>
                  {'Venue:- '}
                  <Text style={{ fontWeight: '' }}>{item.Location}</Text>
                </Text>
              </View>
            ) : null}

            {item.RegestrationPrice ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,
                  marginTop: 15,
                  backgroundColor: ''
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: 'black'
                  }}>
                  {'RegestrationPrice:- '}
                  <Text style={{ fontWeight: '' }}>{item.RegestrationPrice}</Text>
                </Text>
              </View>
            ) : null}

            {item.WiningPrice ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,
                  marginTop: 15,
                  backgroundColor: ''
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: 'black'
                  }}>
                  {'WiningPrice:- '}
                  <Text style={{ fontWeight: '' }}>{item.WiningPrice}</Text>
                </Text>
              </View>
            ) : null}

            {item.Note ? (
              <View
                style={{
                  backgroundColor: '',
                  height: 12,
                  marginTop: 15,
                  backgroundColor: ''
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: 'black'
                  }}>
                  {'Note:- '}
                  <Text style={{ fontWeight: '' }}>{item.Note}</Text>
                </Text>
              </View>
            ) : null}
          </View>

          {item.RegestrationLink ? (
            <View
              style={{
                backgroundColor: '',
                height: 12,
                marginTop: 15,
                justifyContent: 'center',
                alignItems: 'center'
              }}>
              <Text
                onPress={() => Linking.openURL(item.RegestrationLink)}
                numberOfLines={4}
                style={{
                  fontSize: 20,
                  marginTop: 10,
                  color: 'orange',
                  fontWeight: 'bold'
                }}>
                Click To Register
              </Text>
            </View>
          ) : null}

          {item.EventDescription ? (
            <View
              style={{
                backgroundColor: '',
                marginTop: 15,
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center'
              }}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: 'black'
                }}>
                {'Event Details:- '}
                <Text style={{ fontWeight: '', color: 'grey' }}>{item.EventDescription}</Text>
              </Text>
            </View>
          ) : null}
        </ScrollView>
      </View>
    );
  }
}
