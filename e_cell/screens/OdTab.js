import React, { Component } from 'react';
import { View, StyleSheet, Button, ScrollView, Picker } from 'react-native';
import t from 'tcomb-form-native';
import { InstantSearch, connectHits, connectStateResults, Configure } from 'react-instantsearch-native';
import algoliasearch from 'algoliasearch';
const Form = t.form.Form;
var department = t.enums(
  {
    ASET: 'ASET',
    AIBAS: 'AIBAS',
    Animation: 'Animation',
    AppliedSciences: 'Applied Sciences',
    Architecture: 'Architecture'
  },
  'department'
);

var semester = t.enums(
  {
    one: '1',
    twp: '2',
    three: '3',
    four: '4',
    five: '5',
    six: '6',
    seven: '7',
    eight: '8',
    nine: '9',
    ten: '10'
  },
  'semester'
);

var asetcourse = t.enums({
  Aeronautical: 'Aeronautical',
  Aerospace: 'Aerospace'
});

var aibascourse = t.enums({
  Bcom: 'B.com',
  BSc: 'B.Sc.'
});

var animationcourse = t.enums({
  BAMultimediaGaming: 'B.A (Multimedia Gaming)',
  BScAnimationVFX: 'B.Sc.(Animation/VFX)'
});

const User = t.struct({
  email: t.String,
  EventCode: t.String,
  name: t.String,
  enrollment: t.String,
  roll: t.String,
  department: department
});

const options = {
  fields: {
    email: {
      error: 'Without an email address how are you going to reset your password when you forget it?'
    },
    EventCode: {
      label: 'Event Code',
      error: "Choose something you use on a dozen other sites or something you won't remember"
    }
  }
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      school: '',
      course: '',
      sem: '',
      eventname: 'Adarsh Testing',
      ipaddress: '47.122.71.234',
      value: {},
      type: t.struct({
        email: t.String,
        EventCode: t.String,
        name: t.String,
        enrollment: t.String,
        roll: t.String,

        department: department,
        semester: semester
      })
    };
  }

  handleSubmit2 = () => {
    const value = this.formRef.getValue();
    alert(JSON.stringify(value));
  };

  handleSubmit = () => {
    //this.setState({ loading: false });
    const value = this.formRef.getValue();
    var obj1 = {
      email: value.email,
      eventname: this.state.eventname,
      EventCode: value.EventCode,
      name: value.name,
      enrollment: value.enrollment,
      roll: value.roll,
      department: value.department,
      course: value.course,
      objectID: this.state.ipaddress,
      createdDate: new Date().toString()
    };

    var index = algoliasearch('5D2DN6ICU4', '439030e02a665fff425bad82365ec189').initIndex(this.state.eventname);

    index.saveObject(obj1);

    alert(JSON.stringify(obj1));
  };

  erasevalue = () => {
    this.setState({ value: null });
  };

  getType = value => {
    if (value.department === 'ASET') {
      return t.struct({
        email: t.String,
        EventCode: t.String,
        name: t.String,
        enrollment: t.String,
        roll: t.String,
        department: department,
        course: asetcourse,
        semester: semester
      });
    } else if (value.department === 'AIBAS') {
      return t.struct({
        email: t.String,
        EventCode: t.String,
        name: t.String,
        enrollment: t.String,
        roll: t.String,
        department: department,
        course: aibascourse,
        semester: semester
      });
    } else if (value.department === 'ANIMATION') {
      return t.struct({
        email: t.String,
        EventCode: t.String,
        name: t.String,
        enrollment: t.String,
        roll: t.String,
        department: department,
        course: animationcourse,
        semester: semester
      });
    } else {
      return t.struct({
        email: t.String,
        EventCode: t.String,
        name: t.String,
        enrollment: t.String,
        roll: t.String,
        testtypr: t.String,
        department: department,
        semester: semester
      });
    }
  };

  onChange = value => {
    const type = value.department !== this.state.value.department ? this.getType(value) : this.state.type;
    this.setState({ value, type });
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ flex: 1, backgroundColor: 'white' }}>
            <Form ref={c => (this.formRef = c)} type={this.state.type} value={this.state.value} onChange={this.onChange} options={options} />
          </View>

          <Button title="Sign Up" onPress={this.handleSubmit} />
          <Button title="erase Up" onPress={this.erasevalue} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff'
  }
});
