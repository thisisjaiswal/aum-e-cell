import React, { Component } from 'react';
import {
  Image,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  StatusBar,
  Platform,
  Modal
} from 'react-native';
import EventDetails from './EventDetails';
import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, Header } from 'native-base';
import { InstantSearch, connectHits, connectStateResults, Configure } from 'react-instantsearch-native';
import ModalView from './ModalView';
const { height } = Dimensions.get('window');
const Hits = connectHits(({ hits, hasMore, refine, navigation }) => {
  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('EventDetails', {
            item: item
          })
        }>
        <View
          style={{
            height: 120,
            width: '100%',
            backgroundColor: 'white',
            marginTop: 5
          }}>
          <View style={{ flexDirection: 'row', flex: 1 }}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center'
              }}>
              <View style={{ height: 100, width: 100 }}>
                <Thumbnail
                  source={{
                    uri: item.Icon
                  }}
                  style={{
                    width: 100,
                    height: 100,
                    borderRadius: 10,
                    borderWidth: 2,
                    borderColor: 'orange'
                  }}
                />
              </View>
            </View>
            <View
              style={{
                flex: 2,
                backgroundColor: 'white',
                marginTop: 20,
                flexDirection: 'column'
              }}>
              <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Text
                  style={{
                    fontSize: 20,
                    color: 'black',
                    fontWeight: 'bold'
                  }}
                  numberOfLines={2}>
                  {item.Name}
                </Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ marginTop: 3 }}>Click For Od's</Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={{ flex: 1, paddingBottom: 60, backgroundColor: 'white' }}>
      <FlatList
        style={{
          width: '100%',
          backgroundColor: 'transparent'
        }}
        scrollEnabled={true}
        data={hits}
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        keyExtractor={item => item.objectID}
      />
    </View>
  );
});

const Content = connectStateResults(({ searchState, searching, searchResults, navigation }) => {
  var hasResults = searchResults && searchResults.nbHits !== 0;
  if (hasResults) {
    return <Hits navigation={navigation} />;
  }

  if (searching)
    return (
      <View
        style={{
          height: 400,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <ActivityIndicator size="large" color="#000000" />
      </View>
    );
  return (
    <View
      style={{
        height: 550,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
      }}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            bottom: 50
          }}>
          <Image
            source={{
              uri: 'https://cdn.dribbble.com/users/672882/screenshots/2314032/growth.gif'
            }}
            style={{ height: 350, width: 300 }}
          />
          <Text style={{ fontSize: 15, bottom: 50 }}>---No Events---</Text>
        </View>
      </View>
    </View>
  );
});

class Ecell extends React.Component {
  constructor() {
    super();
    this.state = {
      refreshing: false,
      isVisible: false
    };
  }

  //let token = await Notifications.getExpoPushTokenAsync();

  componentDidMount() {
    StatusBar.setBackgroundColor('black');
    //let token = await Notifications.getExpoPushTokenAsync();
    //alert(token);
    //let photoUrl = await AsyncStorage.getItem("photoUrl");
    //let userEmail = await AsyncStorage.getItem("userEmail");
    //let name = await AsyncStorage.getItem("name");
    //let givenName = await AsyncStorage.getItem("givenName");
    //alert(givenName);
    //this.setState({ userEmail: userEmail });
    //this.setState({ photoUrl: photoUrl });
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1 }}>
        {/*{this.state.isVisible ? (
          <View style={{ marginTop: 0 }}>
            <Modal
              isVisible={this.state.isVisible}
              style={{
                justifyContent: 'flex-start',
                backgroundColor: 'white',
                marginHorizontal: 0,
                marginBottom: 0,
                marginTop: Platform.OS === 'ios' ? 140 : height * 0.6,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                borderTopLeftRadius: 30,
                borderTopRightRadius: 30,
                overflow: 'hidden'
              }}
              //onBackButtonPress={() => thi1s.setState({ isVisible: false })}
              onBackdropPress={() => this.setState({ isVisible: false })}
              onSwipe={() => this.setState({ isVisible: false })}
              onSwipeThreshold={20}
              swipeDirection="down">
              <ModalView />
            </Modal>
          </View>
        ) : null}*/}

        <View style={{ backgroundColor: 'black', height: 29 }}></View>
        <View>
          <Header style={{ backgroundColor: 'black' }} androidStatusBarColor="#000">
            <Body style={{ backgroundColor: 'black' }}>
              <Text style={{ color: 'orange', fontSize: 20, fontWeight: 'bold' }}> OD'S Section</Text>
            </Body>

            <Right style={{ backgroundColor: 'black' }}>
              <Text> OD'S Section</Text>
            </Right>
          </Header>
        </View>
        <InstantSearch appId="5D2DN6ICU4" apiKey="439030e02a665fff425bad82365ec189" indexName="Events">
          <Configure />

          <ScrollView showsVerticalScrollIndicator={false}>
            <Content navigation={navigation} />
          </ScrollView>
        </InstantSearch>
      </View>
    );
  }
}

export default Ecell;
