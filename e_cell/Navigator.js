import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Home from './screens/Home';
import OdTab from './screens/OdTab';
import Gallery from './screens/Gallery';
import Articles from './screens/Articles';
class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Home />
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

const TabNavigator = createBottomTabNavigator({
  Home: { screen: HomeScreen },
  Gallery: { screen: Gallery },
  Articles: { screen: Articles },
  Settings: { screen: OdTab }
});

export default createAppContainer(TabNavigator);
